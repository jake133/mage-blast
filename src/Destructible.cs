using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Destructible : MonoBehaviour
{
    protected Rigidbody2D rB2D;
    protected Camera cam;
    protected AudioSoundFX audioSoundFX;
    protected int id;
    protected int hitCount;

    // Use this for initialization
    protected void Inits()
    {
        cam = FindObjectOfType<Camera>();
        audioSoundFX = FindObjectOfType<AudioSoundFX>();

        rB2D = GetComponent<Rigidbody2D>();

        id = this.GetInstanceID();
    }

    void OnTriggerEnter2D(Collider2D trigger)
    {
        if (id != trigger.GetComponent<Spell>().GetCaster())
        {
            hitCount--;

            if (hitCount < 1)
            {
                //    audioSoundFX.PlayOnDeathFX();
                Destroy(gameObject);
            }
            else
            {
             //   audioSoundFX.PlayOnHitFX();
            }
        }
    }
}
