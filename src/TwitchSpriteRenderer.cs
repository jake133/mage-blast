using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TwitchSpriteRenderer : MonoBehaviour {
    public SpriteRenderer thisImage;
    public float sizeFactor;
    public float timeFactor;

    private float timeCounter;
    private bool timeCounterUp;

    // Use this for initialization
    void Start ()
    {
        timeCounter = 1f;
        timeCounterUp = false;

        //timeFactor = 20f;
        //sizeFactor = 5;

        thisImage = this.GetComponent<SpriteRenderer>();
    }
	
	// Update is called once per frame
	void Update ()
    {
        if (timeCounterUp)
        {
            timeCounter += Time.deltaTime / timeFactor;
            if (timeCounter > 1.01)
            {
                timeCounterUp = false;
            }
        }
        else
        {
            timeCounter -= Time.deltaTime / timeFactor;
            if (timeCounter < .99)
            {
                timeCounterUp = true;
            }
        }

        thisImage.transform.localScale = new Vector3(1 + ((1 - timeCounter) * sizeFactor), 1 + ((1 - timeCounter) * sizeFactor), 1);
        //thisImage.color = new Color(1, 1, (1.01f - timeCounter) * sizeFactor * 10, 1);
    }
}
