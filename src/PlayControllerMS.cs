using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayControllerMS : MonoBehaviour
{
    public AccessSaveData accessSaveData;
    public LevelCompleteCanvas levelCompleteCanvas;
    public MageEntrySetup mageEntrySetup;
    public MusicPlayer musicPlayer;
    public DebugLog debugLog;

    public CharM3_Mage charM3_Mage;
    public MageControllerMS prefabMageController;

    private int mageArrayCount;

    private float turnDuration = 2;
    private float turnTimer;
    private bool movedThisTurnAlready;
    private bool playingRegMusic;
    private bool gameOver;

    private bool forceCombat;

    private CharM3_Mage[] physicalMage;
    private MageControllerMS[] mageControllerMS;
    private int[] id;
    private GameObject[] nameTag;

    public GameStats gameStats;
    public GameObject nameLabelPrefab;

    // Start is called before the first frame update
    void Start()
    {
        // this needs to coordinate with CountDownTimer class
        turnTimer = -6;
        mageArrayCount = 11;

        id = new int[mageArrayCount];
        nameTag = new GameObject[mageArrayCount];
        physicalMage = new CharM3_Mage[mageArrayCount];
        mageControllerMS = new MageControllerMS[mageArrayCount];
        movedThisTurnAlready = false;
        gameOver = false;
        forceCombat = false;

        musicPlayer = FindObjectOfType<MusicPlayer>();
        playingRegMusic = true;

        for (int i = 0; i < mageArrayCount; i++)
        {
            if (mageEntrySetup.BucketActive(i))
            {
                physicalMage[i] = Instantiate(charM3_Mage, new Vector3(5, 5, 1), transform.rotation);
                mageControllerMS[i] = Instantiate(prefabMageController, new Vector3(5, 5, 1), transform.rotation);

                if (mageEntrySetup.BucketUseBot(i))
                {   // bucket [0] is the default dummy JS.  Access [0] if the user wants a BOT and not a custom mage script
                    mageControllerMS[i].SetScripts(accessSaveData.GetJS(0));
                }
                else
                {
                    mageControllerMS[i].SetScripts(accessSaveData.GetJS(i));
                }

                mageControllerMS[i].Start();
                id[i] = physicalMage[i].GetId();
                SetNameTag(i);
                gameStats.InitMage(id[i], accessSaveData.GetMageName(i));
                mageControllerMS[i].SetName(accessSaveData.GetMageName(i));
            }
        }

        RandomizeMageLocation();
        CheckForSoloPlayer();

    }

    private void CheckForSoloPlayer()
    {
        int mageCount = 0;
        int soloMageID = 0;

        for (int i = 0; i < mageArrayCount; i++)
        {
            if (mageEntrySetup.BucketActive(i))
            {
                mageCount++;
                soloMageID = id[i];
            }
        }

        // if only 1 mage enterd, set Rank count to ensure level complete canvas populates correctly
        if (mageCount == 1)
        {
            gameStats.SetRank(soloMageID);
        }
    }

    private void RandomizeMageLocation()
    {
        Vector2 center = FindObjectOfType<Camera>().transform.position;
        List<float> orderInCircle = new List<float>();
        int radius = 2;
        int mageCount = 0;
        //int activatedMageCounter = 1;

        for (int i = 0; i < mageArrayCount; i++)
        {
            if (mageEntrySetup.BucketActive(i))
            {
                mageCount++;
                orderInCircle.Add(mageCount);
            }
        }

        float temp = 0;
        int bucket = 0;

        for (int i = 0; i < orderInCircle.Count; i++)
        {
            bucket = UnityEngine.Random.Range(0, orderInCircle.Count);
            temp = orderInCircle[i];
            orderInCircle[i] = orderInCircle[bucket];
            orderInCircle[bucket] = temp;
       }

        bucket = 0;

        for (int i = 0; i < mageArrayCount; i++)
        {
            if (mageEntrySetup.BucketActive(i))
            {
                physicalMage[i].transform.position = new Vector2(
                    center.x + (float)(Math.Cos(2* Math.PI * orderInCircle[bucket] / mageCount) * radius),
                    center.y + (float)(Math.Sin(2* Math.PI * orderInCircle[bucket] / mageCount) * radius));
                bucket++;
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        MoveNameTags();
        turnTimer += Time.deltaTime;

        if (turnTimer > turnDuration)
        {
            if (IsGameOver())
            {
                levelCompleteCanvas.ActivateCompletionCanvas();
            }
            else
            {
                Mages_Reset();
                Mages_LoadData();
                Mages_CalcActions();
                Mages_SpellActions();

                turnTimer = 0;
                movedThisTurnAlready = false;
            }
        }
        else if (turnTimer > (turnDuration / 2) + 0.1f && !movedThisTurnAlready)
        {   // added 0.1f to timer above to allow cast spell to start firing before player moves
            Mages_MoveActions();

            movedThisTurnAlready = true;
        }
        else
        {
            // do nothing
        }
    }

    private bool IsGameOver()
    {
        if (gameOver)
            return true;
        else
        {
            int playersAlive = 0;
            int playerIndex = 77;

            for (int i = 0; i < mageArrayCount; i++)
            {
                if (physicalMage[i] != null)
                    if (physicalMage[i].GetHP() > 0)
                    {
                        playersAlive++;
                        playerIndex = i;
                    }
            }

            if (playersAlive < 3 && playingRegMusic)
            {
                musicPlayer.PlayFinalBattleMusic();
                playingRegMusic = false;
            }

            if (playersAlive > 1)
                return false;
            else
            {
                // last remaining player should stand idle.  77 is just a key to ensure someone is alive
                if (playerIndex != 77)
                    physicalMage[playerIndex].StandStill();

                gameOver = true;
                musicPlayer.PlayBattleMusic();
                return true;
            }
        }
    }

    private void Mages_MoveActions()
    {
        for (int i = 0; i < mageArrayCount; i++)
        {
            if (physicalMage[i] != null)
            {
                if (mageControllerMS[i].GetMoveThisTurn())
                {
                    physicalMage[i].TakeMoveAction(mageControllerMS[i].GetMovementAngle());
                }
                else
                {
                    if (mageControllerMS[i].GetTurnAction() == EAction.nothing)
                        physicalMage[i].AddMana(3);
                }
            }
        }
    }

    public void SetForceCombat()
    {
        FindObjectOfType<PowerUp>().SetSpellPower(2);
        forceCombat = true;
    }

    private void ForcedCombat(int i)
    {
        physicalMage[i].AddMana(10);
        mageControllerMS[i].SetRandomAction(true);
    }

    private void Mages_SpellActions()
    {
        // TODO randomize action order
        // do action
        for (int i = 0; i < mageArrayCount; i++)
        {
            if (physicalMage[i] != null)
            {
                if (forceCombat)
                    ForcedCombat(i);

                if (mageControllerMS[i].GetTurnAction() == EAction.blink)
                {
                    // blink spell uses movementAngle instead of attackID as input parameter
                    physicalMage[i].CastBlinkAction(mageControllerMS[i].GetMovementAngle());
                }
                else if (mageControllerMS[i].GetTurnAction() == EAction.iceshield)
                {
                    // iceshield spell uses movementAngle instead of attackID as input parameter
                    physicalMage[i].TakeSpellAction(EAction.iceshield, new Vector2(0, 0));
                }
                else if (mageControllerMS[i].GetTurnAction() == EAction.nothing)
                {
                    // iceshield spell uses movementAngle instead of attackID as input parameter
                    physicalMage[i].TakeSpellAction(EAction.nothing, new Vector2(0, 0));
                }
                else
                {
                    // NON-blink spells use attackID
                    physicalMage[i].TakeSpellAction(mageControllerMS[i].GetTurnAction(),
                        ConvertIDtoLocation(mageControllerMS[i].GetAttackID()));


                    if (mageControllerMS[i].GetAttackID() == 0)
                    {
                        Debug.Log("Mage id: " + id[i]);
                        Debug.Log("mageControllerMS[i].GetTurnAction(): " + mageControllerMS[i].GetTurnAction());
                    }
                }
            }
        }
    }

    private Vector2 ConvertIDtoLocation(int idIn)
    {
        int i = 0;
        bool keepLooping = true;

        while (keepLooping)
        {
            if (idIn == id[i])
                if (physicalMage[i] != null)
                {
                    return new Vector2(physicalMage[i].GetLocation().x, physicalMage[i].GetLocation().y);
                }
                else
                {
                    Debug.Log("Attack ID has already been killed! - ID: " + idIn);
                    return new Vector2(8.5f, 5);
                }
            else
            {
                i++;
                if (i >= id.Length)
                    keepLooping = false;
            }
        }

        return new Vector2(8.5f, 5);
    }

    private void Mages_CalcActions()
    {
        for (int i = 0; i < mageArrayCount; i++)
        {
            if (mageControllerMS[i] != null)
                mageControllerMS[i].ExecuteScripts();
        }
    }

    private void Mages_LoadData()
    {
        // currentMage is used to pull data from mageArray, then loaded back into main array
        MageData currentMage;

        for (int i = 0; i < mageArrayCount; i++)
        {
            currentMage = new MageData();

            if (physicalMage[i] != null)
            {
                currentMage.id = physicalMage[i].GetId();
                currentMage.hp = physicalMage[i].GetHP();
                currentMage.mana = physicalMage[i].GetMana();
                currentMage.x = physicalMage[i].GetLocation().x;
                currentMage.y = physicalMage[i].GetLocation().y;
                currentMage.isShieldOn = physicalMage[i].IsShieldOn();

                mageControllerMS[i].LoadMyMageData(currentMage);
            }
        }

        for (int i = 0; i < mageArrayCount; i++)
        {
            currentMage = new MageData();

            if (physicalMage[i] != null)
            {
                currentMage.id = physicalMage[i].GetId();
                currentMage.hp = physicalMage[i].GetHP();
                currentMage.mana = physicalMage[i].GetMana();
                currentMage.x = physicalMage[i].GetLocation().x;
                currentMage.y = physicalMage[i].GetLocation().y;
                currentMage.isShieldOn = physicalMage[i].IsShieldOn();

                for (int j = 0; j < mageArrayCount; j++)
                {
                    if (mageControllerMS[j] != null)
                    {
                        if (currentMage.id != id[j] && currentMage.hp > 0)
//                        if (currentMage.id != id[j])
                        {
                            //Debug.Log("MagesLoadData: " + currentMage.id + " " + id[j]);
                            mageControllerMS[j].LoadListData(currentMage);
                        }
                    }
                }
            }
        }
    }

    private void MoveNameTags()
    {
        for (int i = 0; i < mageArrayCount; i++)
            if (physicalMage[i] != null)
                nameTag[i].transform.position = physicalMage[i].GetLocation() + new Vector3(0.1f, 0.5f, -3);
    }

    private void Mages_Reset()
    {
        // let each algorithm know a new turn has started
        // call PLUGIN to getAction
        for (int i = 0; i < mageArrayCount; i++)
        {
            if (mageControllerMS[i] != null)
               mageControllerMS[i].NewTurn();
        }
    }

    // *******************************************************************************************************
    //
    // Access methods - These will need updated if more than 10 mages are needed
    //
    //********************************************************************************************************

    private void SetNameTag(int index)
    {
        nameTag[index] = Instantiate(nameLabelPrefab, new Vector3(0,0,-3), transform.rotation);
        nameTag[index].GetComponent<TextMesh>().text = accessSaveData.GetMageName(index);
    }
}
