using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

public class DebugLog : MonoBehaviour
{
    //public InputField logText;
    private string path;

    // Start is called before the first frame update
    void Start()
    {
        //path = Application.persistentDataPath + "/DebugLog.txt";
        //logText.text = path;
        //logText.text = "check the debuffer foo";

        ResetLog();
    }

    public void LogMessage(string message)
    {
        Debug.Log(System.DateTime.Now.ToString() + ":  " + message);
        //File.AppendAllText(path, System.DateTime.Now.ToString() + ":  " + message + Environment.NewLine);
    }

    private void ResetLog()
    {
        //File.WriteAllText(path, "Debug Log created at " + System.DateTime.Now.ToString() + Environment.NewLine);
    }

}
