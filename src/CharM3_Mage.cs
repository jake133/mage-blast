using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharM3_Mage : CharM3
{
    public Spell_Fireball fireball;
    public Spell_Frostbolt frostbolt;
    public Spell_DrainMana drainMana;
    public Spell_SnowBall snowball;
    public GameObject iceShieldChild;

    protected SpriteRenderer iceShieldSprite;
    protected Flash flash;

    protected int iceShieldManaCost;

    //private Spell_IceShield iceShieldClone;

    protected string[] spell_animations;
    protected string[] aoe_animations;

    protected int blinkManaCost;

    // Use this for initialization
    void Start ()
    {
        Inits();

        flash = GetComponentInChildren<Flash>();

        animations = new string[] { "2hand", "2h2", "2h3", "area_casting", "area_cating2", "casting", "casting2",
            "dying", "idle", "jump","run", "walk"};
        spell_animations = new string[] { "2hand", "2h2", "2h3" };
        aoe_animations = new string[] { "area_casting", "area_cating2", "casting", "casting2" };

        hp = 10;
        mana = 10;
        maxHp = hp;
        maxMana = mana;
        ac = 0;

        blinkManaCost = 2;
        iceShieldManaCost = 4;

        iceShieldSprite = iceShieldChild.GetComponent<SpriteRenderer>();
        iceShieldSprite.enabled = false;
    }

    // Update is called once per frame
    void Update ()
    {
        if (cam == null)
        {
            cam = GetComponent<Camera>();
        }
        else
        {
            CheckIfBlinkedOffScreen();
        }
    }

    protected void CheckIfBlinkedOffScreen()
    {
        // if off screen left or right, pull in
        if (Mathf.Abs(transform.position.x - cam.transform.position.x) > 8.5)
        {
            transform.position = new Vector2(
                Mathf.Clamp(transform.position.x, 1, cam.transform.position.x * 2 - 1),
                transform.position.y);
        }

        // if mage is ABOVE the top line
        if (transform.position.y > 7)
        {
            transform.position = new Vector2(
                transform.position.x,
                Mathf.Clamp(transform.position.y, 1.6f, 7));
        }

        // if mage is BELOW the screen
        if (transform.position.y < 1)
        {
            transform.position = new Vector2(
                transform.position.x,
                Mathf.Clamp(transform.position.y, 1.6f, 7));
        }
    }

    public void CastBlinkAction(int angle)
    {
        if (!dead)
        {
            Look(angle);
            rB2D.drag = startingDrag * 100;

            animator.Play(aoe_animations[Random.Range(0, aoe_animations.Length)]);
            StartCoroutine(CastBlink(angle));
        }
    }

    protected override void TakeClassAction(EAction action, Vector2 location)
    {
        if (!dead)
        {
            rB2D.drag = startingDrag * 100;
            LookAtOpponent(location);

            switch (action)
            {
                case EAction.fireball:
                    animator.Play(spell_animations[Random.Range(0, spell_animations.Length)]);
                    StartCoroutine(CastFireball(location));
                    break;

                case EAction.frostbolt:
                    animator.Play(spell_animations[Random.Range(0, spell_animations.Length)]);
                    StartCoroutine(CastFrostbolt(location));
                    break;

                case EAction.snowball:
                    animator.Play(spell_animations[Random.Range(0, spell_animations.Length)]);
                    StartCoroutine(CastSnowBall(location));
                    break;

                case EAction.drainmana:
                    animator.Play(spell_animations[Random.Range(0, spell_animations.Length)]);
                    StartCoroutine(CastSpell_DrainMana(location));
                    break;
                /*
                    case "IceNova":
                    animator.Play(aoe_animations[Random.Range(0, aoe_animations.Length)]);
                    break;
                */
                case EAction.iceshield:
                    animator.Play(aoe_animations[Random.Range(0, aoe_animations.Length)]);
                    StartCoroutine(CastIceShield());
                    break;
                case EAction.nothing:
                    animator.Play("idle");
                    AddMana(2);
                    break;
                default:
                    Debug.Log("TakeClassAction - EAction uncoded!");
                    animator.Play("idle");
                    AddMana(2);
                    break;
            }
        }
    }

    public IEnumerator CastFireball(Vector2 location)
    {
        Spell_Fireball clone;
        yield return new WaitForSeconds(1);

        if (mana < fireball.ManaCost())
        {
            audioSoundFX.PlayFizzleFX();
        }
        else
        {
            if (looking == "right")
            {
                clone = Instantiate(fireball,
                    new Vector3(
                        transform.position.x + 0.5f,
                        transform.position.y,
                        transform.position.z - 1), transform.rotation);
            }
            else
            {
                clone = Instantiate(fireball,
                    new Vector3(
                        transform.position.x - 0.5f,
                        transform.position.y,
                        transform.position.z - 1), transform.rotation);
            }

            audioSoundFX.PlayFireCastFX();
            mana -= fireball.ManaCost();

            yield return new WaitForSeconds(.01f);
            clone.SetCaster(id);
            clone.ShootAt(location);
        }
    }



    public IEnumerator CastFrostbolt(Vector2 location)
    {
        Spell_Frostbolt clone;
        yield return new WaitForSeconds(1);

        if (mana < frostbolt.ManaCost())
        {
            audioSoundFX.PlayFizzleFX();
        }
        else
        {
            if (looking == "right")
            {
                clone = Instantiate(frostbolt,
                    new Vector3(
                        transform.position.x + 0.5f,
                        transform.position.y,
                        transform.position.z - 1), transform.rotation);
            }
            else
            {
                clone = Instantiate(frostbolt,
                    new Vector3(
                        transform.position.x - 0.5f,
                        transform.position.y,
                        transform.position.z - 1), transform.rotation);
            }

            audioSoundFX.PlayFrostBoltFX();
            mana -= frostbolt.ManaCost();

            yield return new WaitForSeconds(.01f);
            clone.SetCaster(id);
            clone.ShootAt(location);
        }
    }

    public IEnumerator CastSpell_DrainMana(Vector2 location)
    {
        Spell_DrainMana clone;
        yield return new WaitForSeconds(1);

        if (mana < drainMana.ManaCost())
        {
            audioSoundFX.PlayFizzleFX();
        }
        else
        {
            if (looking == "right")
            {
                clone = Instantiate(drainMana,
                    new Vector3(
                        transform.position.x + 0.5f,
                        transform.position.y,
                        transform.position.z - 1), transform.rotation);
            }
            else
            {
                clone = Instantiate(drainMana,
                    new Vector3(
                        transform.position.x - 0.5f,
                        transform.position.y,
                        transform.position.z - 1), transform.rotation);
            }
            audioSoundFX.PlayDrainManaFX();
            mana -= drainMana.ManaCost();

            yield return new WaitForSeconds(.01f);
            clone.SetCaster(id);
            clone.ShootAt(location);
        }
    }

    public IEnumerator CastSnowBall(Vector2 location)
    {
        Spell_SnowBall clone;
        yield return new WaitForSeconds(1);

        if (mana < snowball.ManaCost())
        {
            audioSoundFX.PlayFizzleFX();
        }
        else
        {
            if (looking == "right")
            {
                clone = Instantiate(snowball,
                    new Vector3(
                        transform.position.x + 0.5f,
                        transform.position.y,
                        transform.position.z - 1), transform.rotation);
            }
            else
            {
                clone = Instantiate(snowball,
                    new Vector3(
                        transform.position.x - 0.5f,
                        transform.position.y,
                        transform.position.z - 1), transform.rotation);
            }

            audioSoundFX.PlaySnowBallFX();
            mana -= snowball.ManaCost();

            yield return new WaitForSeconds(.01f);
            clone.SetCaster(id);
            clone.ShootAt(location);
        }
    }

    public IEnumerator CastIceShield()
    {
        yield return new WaitForSeconds(1);

        if (mana < iceShieldManaCost)
        {
            audioSoundFX.PlayFizzleFX();
        }
        else
        {
            iceShieldSprite.enabled = true;
            audioSoundFX.PlayIceShieldFX();
            mana -= iceShieldManaCost;
        }
    }

    public IEnumerator CastBlink(int thisAngle)
    {
        yield return new WaitForSeconds(1);

        if (mana < blinkManaCost)
        {
            audioSoundFX.PlayFizzleFX();
        }
        else
        {
            transform.position = transform.position + 3*Direction(thisAngle);

            audioSoundFX.PlayBlinkFX();
            mana -= blinkManaCost;
        }
    }
    protected override void CalculateDamage(Collider2D trigger)
    {
        if (iceShieldSprite.enabled)
        {
            iceShieldSprite.enabled = false;
        }
        else
        {
            hp -= trigger.GetComponent<Spell>().damage;
            mana -= trigger.GetComponent<Spell>().manaBurn;

            hp = Mathf.Clamp(hp, 0, maxHp);
            mana = Mathf.Clamp(mana, 0, maxMana);


            flash.ActivateFlash();
        }
    }

    public bool IsShieldOn()
    {
        return iceShieldSprite.enabled;
    }

    public void AddMana(int manaToAdd)
    {
        if (manaRegenBlocked)
            manaRegenBlocked = false;
        else
        {
            mana += manaToAdd;
            mana = Mathf.Clamp(mana, 0, maxMana);
        }
    }
}
