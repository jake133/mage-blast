using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharM3_Knight_AI : CharM3
{
    private string[] attack_animations;
    private string[] defense_animations;
    private GameObject mageObject;

    public Spell_SwordAttack spell_swordAttack;

    public bool stationary;
    public bool patrol;
    public float patrolTime;

    private bool isAttacking;
    private bool inCombat;
    private bool stunned;
    private Vector3 moveToHere;
    private float direction;


    private IEnumerator decisionRoutine;

    // Use this for initialization
    void Start ()
    {
        Inits();
        animations = new string[] { "attack", "attack3", "attack4", "battlecry",
            "dying", "idle", "jump","run","shield","walk"};
        attack_animations = new string[] { "attack", "attack3", "attack4" };
        defense_animations = new string[] { "battlecry", "shield" };

        hp = 15;
        mana = 10;
        ac = 50;

        isAttacking = false;
        inCombat = false;

        mageObject = GameObject.Find("mage_dark");
        LookAtOpponent(mageObject.transform.position);

        if (patrolTime == 0)
            patrolTime = 1;

        direction = 1;

        decisionRoutine = Decisions();

        StartCoroutine(decisionRoutine);
        StartCoroutine(MoveLeftAndRight());
    }

    private void Update()
    {
        if (dead)
        {
        }
        else if (isAttacking)
        {
        }
        else if (stunned)
        {
        }
        else if (stationary)
        {
        }
        else if (patrol)
        {
            TakeWalkActionWithGravity(direction);

        }
        else if (inCombat)
        {
            if (Mathf.Abs(transform.position.x - moveToHere.x) > 0.1)
                TakeMoveActionWithGravity(moveToHere);
        }
    }

    private IEnumerator Decisions()
    {
        while (!dead)
        {
            yield return new WaitForSeconds(0.25f);
            moveToHere = mageObject.transform.position;

            // assume combat is ON, and only turn off for the below situations
            inCombat = true;

            if (Mathf.Abs(mageObject.transform.position.y - transform.position.y) > 1)
            {   // mage is either above or below
                inCombat = false;
            }
            else if (Mathf.Abs(mageObject.transform.position.x - transform.position.x) > 7)
            {   // mage is too far away
                inCombat = false;
            }
            else if (mageObject.GetComponent<CharM3_Mage_Platformer>().GetHP() < 1)
            {   // player is dead, stop attackin a corpse
                inCombat = false;
            }
            else if (Mathf.Abs(mageObject.transform.position.x - transform.position.x) < 1.25f)
            {   // mage is close enough to attack
                StartCoroutine(Attack());
            }
        }
    }

    private IEnumerator Attack()
    {
        // the !dead is required in the event the knight is killed 
        if (!isAttacking && !dead)
        {
            isAttacking = true;

            Spell_SwordAttack clone;
            switch (Random.Range(1, 4))
            {
                case 1:
                    animator.Play("attack");
                    break;
                case 2:
                    animator.Play("attack3");
                    break;
                default:
                    animator.Play("attack4");
                    break;
            }

            audioSoundFX.PlayFizzleFX();

            yield return new WaitForSeconds(0.25f);

            clone = Instantiate(spell_swordAttack,
                new Vector3(
                    transform.position.x,
                    transform.position.y,
                    transform.position.z), transform.rotation);
            clone.Init();
            clone.SwingAttack();
            clone.SetCaster(id);

            isAttacking = false;
        }
    }

    private IEnumerator Stunned()
    {
        stunned = true;
        yield return new WaitForSeconds(0.25f);
        stunned = false;
    }

    private IEnumerator MoveLeftAndRight()
    {
        while (!dead)
        {
            yield return new WaitForSeconds(patrolTime);

            direction *= -1;
        }
    }

    public override void ApplyFireDamage(int damage, int attackerID)
    {
        if (hp > 0)
        {
            stationary = false;
            patrol = false;
            StopCoroutine(decisionRoutine);
            TakeMoveActionWithGravity(transform.position);
            animator.Play("idle");
            StartCoroutine(Stunned()); ;
            StartCoroutine(decisionRoutine);

            hp -= damage;

            if (hp < 1)
            {
                hp = 0;
                audioSoundFX.PlayOnDeathFX();
                dead = true;
                animator.Play("dying");
                rB2D.gravityScale = 0;
                Destroy(GetComponent<CapsuleCollider2D>());
                GetComponent<SpriteRenderer>().sortingOrder = 1;
            }
            else
            {
                // audioSoundFX.PlayOnHitFX();
            }
        }
    }

    protected override void OnTriggerEnter2D(Collider2D trigger)
    {
        if (hp > 0)
        {
            if (trigger.GetComponent<Spell>() == null)
            { }
            else if (id != trigger.GetComponent<Spell>().GetCaster())
            {
                stationary = false;
                patrol = false;
                StopCoroutine(decisionRoutine);
                TakeMoveActionWithGravity(transform.position);
                animator.Play("idle");
                StartCoroutine(Stunned());
                StartCoroutine(decisionRoutine);

                CalculateDamage(trigger);

                if (hp < 1)
                {
                    audioSoundFX.PlayOnDeathFX();
                    dead = true;
                    rB2D.drag = startingDrag * 100;
                    animator.Play("dying");
                    rB2D.gravityScale = 0;
                    Destroy(GetComponent<CapsuleCollider2D>());
                    GetComponent<SpriteRenderer>().sortingOrder = 1;
                }
                else
                {
                    audioSoundFX.PlayOnHitFX();
                }
            }
        }
    }
}
