using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

/*** Save Data ***
 * Requires: Save Manager to control creating, reading, writing, updating save data
 * Function: data class/copybook/structure use to store all applicable data elements used in a saved game 
 * 
 * Notes: 
 */

[System.Serializable]
public class SaveData
{
    public string mageName;
    public string customJS;
}
