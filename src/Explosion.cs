using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Explosion : MonoBehaviour
{
    public SpriteRenderer flashImage;

    private float elapsedTime;
    private Color startColor;

    private int rotateDirection;

    // Start is called before the first frame update
    void Start()
    {
        elapsedTime = 0;
        startColor = flashImage.color;

        flashImage.color = new Color(1, 1, 1, 0.6f);

        if (Random.Range(1, 3) == 1)
            rotateDirection = 1;
        else
            rotateDirection = -1;
    }

    // Update is called once per frame
    void Update()
    {
        elapsedTime += Time.deltaTime;

        if (flashImage.color.a > 0)
            flashImage.color = new Color(startColor.r, startColor.g, startColor.b,
                flashImage.color.a - (Time.deltaTime));

        if (elapsedTime > 1)
        {
            Destroy(gameObject);
        }
    }

    private void FixedUpdate()
    {
        flashImage.transform.localScale = new Vector3(
        flashImage.transform.localScale.x * 1.01f,
        flashImage.transform.localScale.y * 1.01f, 1);

        flashImage.transform.Rotate(new Vector3(
        flashImage.transform.rotation.x,
        flashImage.transform.rotation.y, rotateDirection));

    }
}
