using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Spell : MonoBehaviour
{
    protected CircleCollider2D circleCollider;
    public ParticleLingering particleLingering;
    private CameraController cameraController;
    private Rigidbody2D rB2D;

    public int casterId;
    public int damage;
    public int manaBurn;
    public int spellSpeed;

    protected Vector3 initialPosition;
    protected Vector3 startPosition;
    protected Vector3 endPosition;
    protected float currentTimeOnPath;
    protected float totalTimeForPath;

    public bool show;

    private Vector2 direction;
    private Vector2 startVector;
    private Vector2 endVector;

    public void SetCaster(int id)
    {
        casterId = id;
    }

    public int GetCaster()
    {
        return casterId;
    }

    protected void MoveBolt()
    {
        currentTimeOnPath += Time.deltaTime;

        if (currentTimeOnPath > totalTimeForPath)
        {
            endVector = this.transform.position;
            // Debug.Log("Spell Traveled Distance: " + Math.Abs(Vector2.Distance(startVector, endVector)));
            DestroySpell();
        }
    }

    public void ShootAt(Vector2 thisEndPoint)
    {
        rB2D = GetComponent<Rigidbody2D>();
        startVector = this.transform.position;

        totalTimeForPath = 1f;
        show = true;
        circleCollider.isTrigger = true;

        direction = thisEndPoint - new Vector2(this.transform.position.x, this.transform.position.y);

        direction.Normalize();

        float spellPower = 1;
        if (FindObjectOfType<PowerUp>() != null)
            spellPower = FindObjectOfType<PowerUp>().GetSpellPower();

        rB2D.velocity = direction * 8 * spellPower;
    }

    void OnTriggerEnter2D(Collider2D trigger)
    {
        if (tag == "Enemy")
        {
            // for spells defined as an Enemy, they are constant and should permanently exist
        }
        else if (trigger.tag == "Grass" || trigger.tag == "Floor" || trigger.tag == "Enemy_Static_Sword")
        {
            //do nothing, its just grass
        }
        else if (trigger.tag == "ScreenEdge" || trigger.tag == "Spell")
        {
            cameraController = FindObjectOfType<CameraController>();

            if (cameraController != null)
                cameraController.ShakeCamera(0.1f, 0.1f);

            DestroySpell();
        }
        else if (trigger.tag == "Env Object")
        {
            cameraController = FindObjectOfType<CameraController>();

            if (cameraController != null)
                cameraController.ShakeCamera(0.1f, 0.1f);

            DestroySpell();
        }
        else if (GetCaster() == trigger.GetComponent<CharM3>().GetId())
        {
            //do nothing, collision with yourself
        }
        else
        {
            cameraController = FindObjectOfType<CameraController>();

            if (cameraController != null)
                cameraController.ShakeCamera(0.1f, 0.1f);

            SpellHitExtras();
            DestroySpell();
        }
    }

    public void DestroySpell()
    {
        if (particleLingering != null)
            particleLingering.AboutToDie();

        Destroy(gameObject);
    }

    protected virtual void SpellHitExtras()
    {
        // nothing
    }
}

