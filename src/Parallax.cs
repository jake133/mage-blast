using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Parallax : MonoBehaviour
{
    public Camera cam;
    public float parallaxAmount;

    private Transform thisLayer;
    private Vector3 startingPosition;

    // Start is called before the first frame update
    void Start()
    {
        thisLayer = GetComponent<Transform>();
        startingPosition = thisLayer.position;
    }

    // Update is called once per frame
    void Update()
    {
        thisLayer.position = new Vector3(
            cam.transform.position.x * parallaxAmount,
            startingPosition.y,
            startingPosition.z); 
    }
}
