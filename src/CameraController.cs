using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    private Vector3 startingPosition;
    private float shakeTimer;

    private float shakeDuration;
    public float shakeAmount = 0.2f;

    public bool followPlayer;
    private CharM3_Mage charM3_Mage;
    private bool isShaking;

    // Start is called before the first frame update
    void Start()
    {
        startingPosition = transform.position;
        shakeDuration = 1;
        isShaking = false;
    }

    private void Update()
    {
        if (followPlayer)
        {
            if (charM3_Mage == null)
                charM3_Mage = FindObjectOfType<CharM3_Mage>();
            else
            {
                transform.localPosition = new Vector3(
                    Mathf.Clamp(transform.localPosition.x,
                    charM3_Mage.transform.position.x - 2, charM3_Mage.transform.position.x + 2),
                    transform.localPosition.y, transform.localPosition.z);
            }

            if (!isShaking)
                startingPosition = transform.position;
        }
    }

    public void ShakeCamera(float duration, float shakeFactor)
    {
        StopCoroutine("Shake");
        transform.position = startingPosition;

        shakeDuration = duration;
        shakeAmount = shakeFactor;

        StartCoroutine("Shake");
    }

    IEnumerator Shake()
    {
        isShaking = true;
        shakeTimer = 0; 

        while (shakeTimer < shakeDuration)
        {
            shakeTimer += Time.deltaTime;
            transform.localPosition = startingPosition + Random.insideUnitSphere * shakeAmount;

            yield return null;
        }
        transform.localPosition = startingPosition;
        isShaking = false;
    }
}
