using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerUp : MonoBehaviour
{
    private float spellPower;
    private float speedMultiplier;

    // Start is called before the first frame update
    void Start()
    {
        spellPower = 1;
        speedMultiplier = 1;
    }

    public float GetSpellPower()
    {
        return spellPower;
    }

    public void SetSpellPower(float power)
    {
        spellPower = power;
    }

    public float GetSpeedMultiplier()
    {
        return speedMultiplier;
    }

    public void SetSpeedMultiplier(float power)
    {
        speedMultiplier = power;
    }
}
