using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spell_DrainMana : Spell
{
    // Start is called before the first frame update
    public void Start()
    {
        circleCollider = GetComponent<CircleCollider2D>();

        circleCollider.isTrigger = false;

        show = false;

        damage = 0;
        manaBurn = 5;
    }

    // Update is called once per frame
    void Update()
    {
        if (show)
            MoveBolt();
    }

    public int ManaCost()
    {
        return 3;
    }

}
