using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharM3 : MonoBehaviour
{
    protected Animator animator;
    protected Rigidbody2D rB2D;
    protected Camera cam;
    protected AudioSoundFX audioSoundFX;
    protected GameStats gameStats;

    public EAction eAction; 
    public float editorSpeedMultiplier;

    protected float time;
    protected string[] animations;

    protected int index;

    protected float startingDrag;

    protected int hp;
    protected float mana;
    protected int ac;

    protected int maxHp;
    protected float maxMana;

    protected string looking;
    protected int id;

    public bool dead;
    protected bool manaRegenBlocked;

    protected float originaScale;

    // Use this for initialization
    protected void Inits()
    {
        animator = GetComponent<Animator>();
        cam = FindObjectOfType<Camera>();
        audioSoundFX = FindObjectOfType<AudioSoundFX>();
        gameStats = FindObjectOfType<GameStats>();

        index = 0;
        rB2D = GetComponent<Rigidbody2D>();
        startingDrag = rB2D.drag;
        looking = "right";
        dead = false;
        manaRegenBlocked = false;
        originaScale = transform.localScale.x;

        id = this.GetInstanceID();
    }

    protected void Look(Vector2 direction)
    {
        if (direction.x > 0) // To the right
        {
            transform.localScale = new Vector3(originaScale, originaScale, originaScale);
            looking = "right";
        }
        else if (direction.x < 0) // To the left
        {
            transform.localScale = new Vector3(-originaScale, originaScale, originaScale);
            looking = "left";
        }
    }

    protected void LookAtOpponent(Vector2 direction)
    {
        if (direction.x > transform.position.x) // To the right
        {
            transform.localScale = new Vector3(originaScale, originaScale, originaScale);
            looking = "right";
        }
        else if (direction.x < transform.position.x) // To the left
        {
            transform.localScale = new Vector3(-originaScale, originaScale, originaScale);
            looking = "left";
        }
    }

    protected void Look(int angle)
    {
        if (angle < 180 && angle > 0) // To the right
        {
            transform.localScale = new Vector3(originaScale, originaScale, originaScale);
            looking = "right";
        }
        else if (angle > 180 && angle < 360) // To the left
        {
            transform.localScale = new Vector3(-originaScale, originaScale, originaScale);
            looking = "left";
        }
    }

    void MoveInThisDirection(Vector2 thisDirection)
    {
        Look(thisDirection);
        rB2D.velocity = thisDirection * 1.5f * editorSpeedMultiplier;
    }

    public void TakeSpellAction(EAction action, Vector2 location)
    {
        if (!dead)
        {
            switch (action)
            {
                default:
                    TakeClassAction(action, location);
                    break;
            };
        }
    }

    public void TakeMoveAction(int details)
    {
        if (!dead)
        {
            animator.Play("run");
            rB2D.drag = startingDrag;
            MoveInThisDirection(Direction(details));
        }
    }

    public void TakeMoveAction(Vector3 location)
    {
        if (!dead)
        {
            animator.Play("run");
            rB2D.drag = startingDrag;
            MoveInThisDirection(location);
        }
    }

    public void TakeMoveActionWithGravity(float xSpeed)
    {
        if (!dead)
        {
            animator.Play("run");
            rB2D.drag = startingDrag;
            MoveInThisDirectionWithGravity(xSpeed);
        }
    }

    public void TakeMoveActionWithGravity(Vector3 towardsVector)
    {
        if (!dead)
        {
            animator.Play("run");
            rB2D.drag = startingDrag;

            if (towardsVector.x > transform.position.x)
                MoveInThisDirectionWithGravity(1);
            else if (towardsVector.x < transform.position.x)
                MoveInThisDirectionWithGravity(-1);
        }
    }

    public void TakeWalkActionWithGravity(float xSpeed)
    {
        if (!dead)
        {
            animator.Play("walk");
            rB2D.drag = startingDrag;
            MoveInThisDirectionWithGravity(xSpeed/2);
        }
    }

    void MoveInThisDirectionWithGravity(float xSpeed)
    {
        Look(new Vector2(xSpeed, 0));
        rB2D.velocity = new Vector2(xSpeed * editorSpeedMultiplier, rB2D.velocity.y);
    }

    public void StandStill()
    {
        if (!dead)
        {
            animator.Play("idle");
            rB2D.drag = startingDrag * 100;
        }
    }

    public void Jump()
    {
        if (!dead)
        {
            animator.Play("jump");
            rB2D.drag = startingDrag;
            rB2D.AddForce(new Vector2(0,450));
        }
    }

    public void ForceWhileJumping(float angle)
    {
        Look(new Vector2(angle, 0));
        rB2D.velocity = new Vector2(angle * editorSpeedMultiplier, rB2D.velocity.y);

    }

    protected virtual void TakeClassAction(EAction action, Vector2 location)
    {
        Debug.Log("TakeClassAction not overridden - check child class");
        animator.Play("idle");
        rB2D.drag = startingDrag * 100;
    }

    protected virtual void OnTriggerEnter2D(Collider2D trigger)
    {
        if (hp > 0)
        {
            if (id != trigger.GetComponent<Spell>().GetCaster())
            {
                CalculateDamage(trigger);

                if (hp < 1)
                {
                    gameStats.AddKillCount(trigger.GetComponent<Spell>().GetCaster());
                    gameStats.SetRank(id);
                    audioSoundFX.PlayOnDeathFX();
                    dead = true;
                    rB2D.drag = startingDrag * 100;
                    animator.Play("dying");
                    Destroy(GetComponent<BoxCollider2D>());
                }
                else
                {
                    audioSoundFX.PlayOnHitFX();
                }
            }
        }
    }

    protected virtual void CalculateDamage(Collider2D trigger)
    {
        hp -= trigger.GetComponent<Spell>().damage;
        mana -= trigger.GetComponent<Spell>().manaBurn;

        if (hp < 0)
            hp = 0;
        if (mana < 0)
            mana = 0;

        // prevents the next mana regen tick from occurring after a mana burn
        if (trigger.GetComponent<Spell>().manaBurn > 0)
            manaRegenBlocked = true;
    }

    public int GetId()
    {
        id = this.GetInstanceID();
        return id;
    }

    public int GetHP()
    {
        return hp;
    }

    public int GetMana()
    {
        return (int)mana;
    }

    public int GetMaxHp()
    {
        return maxHp;
    }

    public int GetMaxMana()
    {
        return (int)maxMana;
    }

    public Vector3 GetLocation()
    {
        return transform.position;
    }

    protected Vector3 Direction(int angle)
    {
        while (angle > 360)
        {
            angle = angle - 360;
        }

        Vector3 returnVector;

        if (angle < 23)
            returnVector = new Vector3(0, 1);
        else if (angle < 68)
            returnVector = new Vector3(1, 1);
        else if (angle < 113)
            returnVector = new Vector3(1, 0);
        else if (angle < 158)
            returnVector = new Vector3(1, -1);
        else if (angle < 203)
            returnVector = new Vector3(0, -1);
        else if (angle < 248)
            returnVector = new Vector3(-1, -1);
        else if (angle < 293)
            returnVector = new Vector3(-1, 0);
        else if (angle < 338)
            returnVector = new Vector3(-1, 1);
        else
            returnVector = new Vector3(0, 1);

        returnVector.Normalize();
        return returnVector;
    }

    public virtual void ApplyFireDamage(int damage, int attackerID)
    {
        if (hp > 0)
        {

            hp -= damage;

            if (hp < 1)
            {
                hp = 0;
                gameStats.AddKillCount(attackerID);
                gameStats.SetRank(id);
                audioSoundFX.PlayOnDeathFX();
                dead = true;
                rB2D.drag = startingDrag * 100;
                animator.Play("dying");
                Destroy(GetComponent<BoxCollider2D>());
            }
            else
            {
                // audioSoundFX.PlayOnHitFX();
            }
        }
    }
}
