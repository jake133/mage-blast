using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spell_Frostbolt : Spell
{
    private Transform frostboltImage;

    private int angle;

    // Start is called before the first frame update
    public void Start()
    {
        frostboltImage = GetComponent<Transform>();
        circleCollider = GetComponent<CircleCollider2D>();

        circleCollider.isTrigger = false;
        angle = 1;
        //initialPosition = transform.position;
        show = false;

        damage = 5;
    }

    // Update is called once per frame
    void Update()
    {
        angle++;
        if (angle > 360)
            angle = 1;

        // 2.5 rotations/sec - 900 degrees
        frostboltImage.Rotate(new Vector3(
            frostboltImage.rotation.x, 
            frostboltImage.rotation.y,
            900 * Time.deltaTime));

        if (show)
            MoveBolt();
    }

    public int ManaCost()
    {
        return 5;
    }
}
