using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using System.IO;

using MoonSharp.Interpreter;
using MoonSharp.Interpreter.Serialization;

//using ServiceStack.Script;

public class MageControllerMS : MonoBehaviour
{
    public DebugLog debugLog;

    private EAction action;
    private int movementAngle;
    private int attackID;

    protected List<MageData> currentMageData = new List<MageData>();
    public MageData myMage;
    private System.Random randomNum;
    private bool takeMoveAction;

    private string mageName;

    private string moonScript = @"";
    private Script moonScriptEngine;

    // Start is invoked once during program load
    public void Start()
    {

        // Register script file - According to MoonScript Unity3D tips:
        // "By using this little snippet at the very start of your project 
        // you don�t need to add Unity own libraries to link.xml to run 
        // on IL2CPP platforms"
        //Script.DefaultOptions.ScriptLoader =
        //    new MoonSharp.Interpreter.Loaders.UnityAssetsScriptLoader(moonScript);

        debugLog = FindObjectOfType<DebugLog>();
        myMage = new MageData();

        // Set sandbox env = A sort of "hard" sandbox preset, including:
        // string, math, table, bit32 packages, constants and table iterators.
        moonScriptEngine = new Script(CoreModules.Preset_HardSandbox);

        // Hardwiring - For moonscript.  Do not know if this actually helps at all or not
        Table dump = UserData.GetDescriptionOfRegisteredTypes(true);
        File.WriteAllText(Application.persistentDataPath + "/testdump.lua", dump.Serialize());

        // functions with void return type
        moonScriptEngine.Globals["SetAbilityAction"] = (Action<string, int>)SetAbilityAction;
        moonScriptEngine.Globals["SetMoveAction"] = (Action<bool, int>)SetMoveAction;
        moonScriptEngine.Globals["SetRandomAction"] = (Action<bool>)SetRandomAction;
        moonScriptEngine.Globals["SetRandomMove"] = (Action<bool>)SetRandomMove;
        moonScriptEngine.Globals["DebugLog"] = (Action<string>)DebugLog;

        // functions that return something
        moonScriptEngine.Globals["GetCurrentOpponentCount"] = (Func<int>)GetCurrentOpponentCount;
        moonScriptEngine.Globals["GetClosestOpponent_ID"] = (Func<int>)GetClosestOpponent_ID;
        moonScriptEngine.Globals["GetLowestHealthOpponent_ID"] = (Func<int>)GetLowestHealthOpponent_ID;

        NewTurn();
        randomNum = new System.Random(DateTime.Now.Millisecond);
    }

    public void SetName(string nameIn)
    {
        mageName = nameIn;
    }

    public void DebugLog(string printThis)
    {
        debugLog.LogMessage(ConvertToLen11(mageName) + " " + printThis);
    }

    public void SetAbilityAction(string actionIn, int dataIn)
    {
        switch (actionIn.ToLower())
        {
            case "blink":
                action = EAction.blink;
                movementAngle = dataIn;
                break;
            case "drainmana":
                action = EAction.drainmana;
                attackID = dataIn;
                break;
            case "fireball":
                action = EAction.fireball;
                attackID = dataIn;
                break;
            case "frostbolt":
                action = EAction.frostbolt;
                attackID = dataIn;
                break;
            case "iceshield":
                action = EAction.iceshield;
                break;
            case "snowball":
                action = EAction.snowball;
                attackID = dataIn;
                break;
            default:
                action = EAction.nothing;
                break;
        }
    }

    public void SetMoveAction(bool move, int angle)
    {
        takeMoveAction = move;
        movementAngle = angle;
    }

    public int GetCurrentOpponentCount()
    {
        int aliveCount = 0;

        for (int i = 0; i < currentMageData.Count; i++)
        {
            if (currentMageData[i].hp > 0)
                aliveCount++;
        }
        return aliveCount;
    }

    public int GetClosestOpponent_ID()
    {
        MageData closestOpponent = currentMageData[0];

        float closestDistance = 1000;
        Vector2 opponentPosition;
        Vector2 myPosition = new Vector2((float)myMage.x, (float)myMage.y);

        for (int i = 0; i < currentMageData.Count; i++)
        {
            opponentPosition = new Vector2((float)currentMageData[i].x, (float)currentMageData[i].y);

            if (Vector2.Distance(myPosition, opponentPosition) < closestDistance)
            {
                closestOpponent = currentMageData[i];
                closestDistance = Vector2.Distance(myPosition, opponentPosition);
            }
        }

        return closestOpponent.id;
    }

    public int GetLowestHealthOpponent_ID()
    {
        MageData lowestOpponent = currentMageData[0];

        float lowestHealth = 100;

        for (int i = 0; i < currentMageData.Count; i++)
        {
            if (currentMageData[i].hp < lowestHealth)
            {
                lowestOpponent = currentMageData[i];
                lowestHealth = currentMageData[i].hp;
            }
        }

        return lowestOpponent.id;
    }

    public void SetRandomAction(bool whatevs)
    {
        switch (randomNum.Next(1, 6))
        {
            case 1:
                //action = EAction.blink;
                //SelectRandomAngle();
                action = EAction.fireball;
                SelectRandomTarget();
                break;
            case 2:
                action = EAction.drainmana;
                SelectRandomTarget();
                break;
            case 3:
                action = EAction.frostbolt;
                SelectRandomTarget();
                break;
            case 4:
                //action = EAction.iceshield;
                action = EAction.frostbolt;
                SelectRandomTarget();
                break;
            case 5:
                action = EAction.snowball;
                SelectRandomTarget();
                break;
            default:
                action = EAction.nothing;
                SelectRandomTarget();
                break;
        }
    }

    public void SetRandomMove(bool whatevs)
    {
        takeMoveAction = true;
        SelectRandomAngle();
    }

    /********************************************************************/
    /********************************************************************/
    /********************************************************************/

    #region INTERNAL SCRIPTS USED WITHIN C#

    /********************************************************************/
    /********************************************************************/
    /********************************************************************/

    public void SetScripts(string scriptIn)
    {
        moonScript = scriptIn;
    }

    // LoadListData will be invoked multiple times per turn, once for each mage left alive (NOT including yourself)
    public void LoadListData(MageData inData)
    {
        //Debug.Log("LoadListData:" + myMage.id + " " + inData.id);
        currentMageData.Add(inData);
    }

    public void LoadMyMageData(MageData inData)
    {
        //Debug.Log("LoadMyMageData " + inData.id);
        myMage = inData;
    }

    // NewTurn is invoked at the start of each turn.  Reset anything you'd like, or maybe make a copy of the prior
    // list to use for trending purposes?
    public void NewTurn()
    {
        currentMageData = new List<MageData>();

        action = EAction.nothing;
        takeMoveAction = false;
        movementAngle = 0;
        attackID = 0;
    }

    public void ExecuteScripts()
    {
        SetGlobalVariablesInMS();

        try
        {
            DynValue scriptOutput = moonScriptEngine.DoString(moonScript);
        }

        catch (ScriptRuntimeException e)
        {
            debugLog.LogMessage("MoonScript ScriptRuntimeException caught: " + e);
            Debug.Log("MoonScript ScriptRuntimeException caught: " + e);
        }
        catch (SyntaxErrorException e)
        {
            debugLog.LogMessage("Syntax Error Exception caught: " + e);
            Debug.Log("Syntax Error Exception caught: " + e);
        }
    }

    public void ExecuteScripts_Raw(string testScript)
    {
        SetGlobalVariablesInMS();
        DynValue scriptOutput = moonScriptEngine.DoString(testScript);
    }


    private void SetGlobalVariablesInMS()
    {
        UserData.RegisterType<MageData>();
        UserData.RegisterType<Data>();

        Data data = new Data();
        data.BuildMageData(myMage, currentMageData.ToArray());
        moonScriptEngine.Globals["data"] = data;

        moonScriptEngine.Globals["testString"] = "test string";
        moonScriptEngine.Globals["testInt"] = 7;
    }

    public EAction GetTurnAction()
    {
        return action;
    }

    public int GetAttackID()
    {
        return attackID;
    }

    public bool GetMoveThisTurn()
    {
        return takeMoveAction;
    }

    public int GetMovementAngle()
    {
        return movementAngle;
    }

    private void SelectRandomAngle()
    {
        movementAngle = UnityEngine.Random.Range(1, 361);
    }

    private void SelectRandomTarget()
    {
        //Debug.Log("SelectRandomTarget currentMageData.Count: " + currentMageData.Count);

        int randNumber = UnityEngine.Random.Range(0, currentMageData.Count);
        attackID = currentMageData[randNumber].id;
    }

    public int GetMageID()
    {
        return myMage.id;
    }

    private string ConvertToLen11(string inputString)
    {
        string returnString = inputString;

        while (returnString.Length < 10)
        {
            returnString += " ";
        }

        return returnString + ": ";
    }

    public void CleanMage()
    {
        Destroy(gameObject);
    }
    #endregion
}

