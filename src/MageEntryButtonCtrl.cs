using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MageEntryButtonCtrl : MonoBehaviour
{
    private Image blackoutImage;

    void Start()
    {
        blackoutImage = GetComponent<Image>();

        MageSlotInactive();
    }

    public void MageSlotActive()
    {
        blackoutImage.enabled = false;
    }

    public void MageSlotInactive()
    {
        blackoutImage.enabled = true;
    }
}