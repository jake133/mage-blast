using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharM3_Mage_Platformer : CharM3_Mage
{
    private bool isJumping;
    private bool attackOnCD;

    //private float manaF;
    //private float maxManaF;

    public PowerUp powerUp;
    public Shadow shadow;

    private float priorY;

    // Use this for initialization
    void Start ()
    {
        Inits();
        DefaultStartFromM3_Mage();

        StartCoroutine(ManaRegen());
        isJumping = false;
        attackOnCD = false;
        priorY = transform.position.y;
    }

    private void DefaultStartFromM3_Mage()
    {
        flash = GetComponentInChildren<Flash>();
        powerUp.SetSpellPower(2);

        animations = new string[] { "2hand", "2h2", "2h3", "area_casting", "area_cating2", "casting", "casting2",
            "dying", "idle", "jump","run", "walk"};
        spell_animations = new string[] { "2hand", "2h2", "2h3" };
        aoe_animations = new string[] { "area_casting", "area_cating2", "casting", "casting2" };

        hp = 10;
        mana = 10;
        maxHp = hp;
        maxMana = mana;
        ac = 0;

        blinkManaCost = 2;
        iceShieldManaCost = 4;

        iceShieldSprite = iceShieldChild.GetComponent<SpriteRenderer>();
        iceShieldSprite.enabled = false;
    }

    private IEnumerator ManaRegen()
    {
        while (!dead)
        {
            yield return new WaitForSeconds(0.05f);

            mana += 0.1f;
            mana = Mathf.Clamp(mana, 0, maxMana);
        }
    }

    private IEnumerator CheckAttackCD(string spell)
    {
        spell = spell.ToLower();

        if (!attackOnCD)
        {
            Vector2 inFrontOfMage;

            // +2 is used because the spell is instantiated at +1
            if (looking == "right")
            {
                inFrontOfMage = new Vector2(transform.position.x + 2,
                    transform.position.y);
            }
            else
            {
                inFrontOfMage = new Vector2(transform.position.x - 2,
                    transform.position.y);
            }
            attackOnCD = true;

            switch (spell)
                {
                case "snowball":
                    StartCoroutine(CastSnowBall(inFrontOfMage));
                    break;
                case "frostbolt":
                    StartCoroutine(CastFrostbolt(inFrontOfMage));
                    break;
                case "fireball":
                    StartCoroutine(CastFireball(inFrontOfMage));
                    break;
                default:
                    break;
            }

            yield return new WaitForSeconds(0.5f);
            attackOnCD = false;
        }
        yield return true;
    }

    // Update is called once per frame
    void Update ()
    {
        if (Input.GetButtonDown("Fire1"))
            StartCoroutine(CheckAttackCD("frostbolt"));
        else if (Input.GetButtonDown("Fire2"))
            StartCoroutine(CheckAttackCD("snowball"));
        else if (Input.GetButtonDown("Fire3"))
            StartCoroutine(CheckAttackCD("fireball"));

        gameObject.layer = 8; // player chars

        if (isJumping)
        {
            if (priorY < transform.position.y)
                gameObject.layer = 12; // background
            else
                gameObject.layer = 8; // player chars
        }
        priorY = transform.position.y;

        Movement();
    }

    private void Movement()
    {
        if (!isJumping)
        {
            if (Input.GetAxis("Horizontal") > 0)
            {
                TakeMoveActionWithGravity(Input.GetAxis("Horizontal"));
            }
            else if (Input.GetAxis("Horizontal") < 0)
            {
                TakeMoveActionWithGravity(Input.GetAxis("Horizontal"));
            }
            else if (Input.GetAxis("Horizontal") == 0)
            {
                StandStillWithGravity();
            }

            if (Input.GetButtonDown("Jump"))
            {
                Jump();
                isJumping = true;
            }
        }
        else
        {
            // this call doesn't play a new animation
            ForceWhileJumping(Input.GetAxis("Horizontal"));
        }
    }

    public void StandStillWithGravity()
    {
        if (!dead)
        {
            animator.Play("idle");
            rB2D.drag = startingDrag;
            rB2D.velocity = new Vector2(0, rB2D.velocity.y);
        }
    }

    public void NoLongerJumping()
    {
        // this is invoked by teh FLOOR when a player lands
        isJumping = false;
        shadow.OnNewFloor();
    }

    new public IEnumerator CastFireball(Vector2 location)
    {
        Spell_Fireball clone;

        if (mana < fireball.ManaCost())
        {
            audioSoundFX.PlayFizzleFX();
        }
        else
        {
            if (looking == "right")
            {
                clone = Instantiate(fireball,
                    new Vector3(
                        transform.position.x,
                        transform.position.y,
                        transform.position.z), transform.rotation);
            }
            else
            {
                clone = Instantiate(fireball,
                    new Vector3(
                        transform.position.x,
                        transform.position.y,
                        transform.position.z), transform.rotation);
            }

            audioSoundFX.PlayFireCastFX();
            mana -= fireball.ManaCost();

            clone.Start();
            yield return null;
            // yield return new WaitForSeconds(.01f);
            clone.SetCaster(id);
            clone.ShootAt(location);
        }
    }



    new public IEnumerator CastFrostbolt(Vector2 location)
    {
        Spell_Frostbolt clone;

        if (mana < frostbolt.ManaCost())
        {
            audioSoundFX.PlayFizzleFX();
        }
        else
        {
            if (looking == "right")
            {
                clone = Instantiate(frostbolt,
                    new Vector3(
                        transform.position.x,
                        transform.position.y,
                        transform.position.z - 1), transform.rotation);
            }
            else
            {
                clone = Instantiate(frostbolt,
                    new Vector3(
                        transform.position.x,
                        transform.position.y,
                        transform.position.z - 1), transform.rotation);
            }

            audioSoundFX.PlayFrostBoltFX();
            mana -= frostbolt.ManaCost();

            clone.Start();
            yield return null;
            // yield return new WaitForSeconds(.01f);
            clone.SetCaster(id);
            clone.ShootAt(location);
        }
    }

    new public IEnumerator CastSpell_DrainMana(Vector2 location)
    {
        Spell_DrainMana clone;
        yield return new WaitForSeconds(1);

        if (mana < drainMana.ManaCost())
        {
            audioSoundFX.PlayFizzleFX();
        }
        else
        {
            if (looking == "right")
            {
                clone = Instantiate(drainMana,
                    new Vector3(
                        transform.position.x,
                        transform.position.y,
                        transform.position.z - 1), transform.rotation);
            }
            else
            {
                clone = Instantiate(drainMana,
                    new Vector3(
                        transform.position.x,
                        transform.position.y,
                        transform.position.z - 1), transform.rotation);
            }
            audioSoundFX.PlayDrainManaFX();
            mana -= drainMana.ManaCost();

            clone.Start();
            yield return null;
            // yield return new WaitForSeconds(.01f);
            clone.SetCaster(id);
            clone.ShootAt(location);
        }
    }

    new public IEnumerator CastSnowBall(Vector2 location)
    {
        Spell_SnowBall clone;

        if (mana < snowball.ManaCost())
        {
            audioSoundFX.PlayFizzleFX();
        }
        else
        {
            if (looking == "right")
            {
                clone = Instantiate(snowball,
                    new Vector3(
                        transform.position.x,
                        transform.position.y,
                        transform.position.z - 1), transform.rotation);
            }
            else
            {
                clone = Instantiate(snowball,
                    new Vector3(
                        transform.position.x,
                        transform.position.y,
                        transform.position.z - 1), transform.rotation);
            }

            audioSoundFX.PlaySnowBallFX();
            mana -= snowball.ManaCost();

            clone.Start();
            yield return null;
            // yield return new WaitForSeconds(.01f);
            clone.SetCaster(id);
            clone.ShootAt(location);
        }
    }

    new public IEnumerator CastIceShield()
    {
        yield return new WaitForSeconds(1);

        if (mana < iceShieldManaCost)
        {
            audioSoundFX.PlayFizzleFX();
        }
        else
        {
            iceShieldSprite.enabled = true;
            audioSoundFX.PlayIceShieldFX();
            mana -= iceShieldManaCost;
        }
    }

    new public IEnumerator CastBlink(int thisAngle)
    {
        yield return new WaitForSeconds(1);

        if (mana < blinkManaCost)
        {
            audioSoundFX.PlayFizzleFX();
        }
        else
        {
            transform.position = transform.position + 3*Direction(thisAngle);

            audioSoundFX.PlayBlinkFX();
            mana -= blinkManaCost;
        }
    }
    protected override void CalculateDamage(Collider2D trigger)
    {
        if (iceShieldSprite.enabled)
        {
            iceShieldSprite.enabled = false;
        }
        else
        {
            hp -= trigger.GetComponent<Spell>().damage;
            mana -= trigger.GetComponent<Spell>().manaBurn;

            hp = Mathf.Clamp(hp, 0, maxHp);
            mana = Mathf.Clamp(mana, 0, maxMana);


            flash.ActivateFlash();
        }
    }

    new public void AddMana(int manaToAdd)
    {
        if (manaRegenBlocked)
            manaRegenBlocked = false;
        else
        {
            mana += manaToAdd;
            mana = Mathf.Clamp(mana, 0, maxMana);
        }
    }

    new public float GetMana()
    {
        return mana;
    }

    new public float GetMaxMana()
    {
        return maxMana;
    }

    protected override void OnTriggerEnter2D(Collider2D trigger)
    {
        if (hp > 0)
        {
            if (trigger.tag == "Enemy" || id != trigger.GetComponent<Spell>().GetCaster())
            {
                CalculateDamage(trigger);

                if (hp < 1)
                {
                    audioSoundFX.PlayOnDeathFX();
                    dead = true;
                    //rB2D.drag = startingDrag * 100;
                    TakeMoveActionWithGravity(transform.position);
                    animator.Play("dying");
                    GetComponent<CapsuleCollider2D>().size = new Vector3(0.1f, 0.1f, 1);
                }
                else
                {
                    audioSoundFX.PlayOnHitFX();
                }
            }
        }
    }
}
