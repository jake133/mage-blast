using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelManager : MonoBehaviour {

    public void LoadLevel(string name)
    {
        if (name.ToLower() == "menu")
            FindObjectOfType<MusicPlayer>().PlayMenuMusic();

        if (name.ToLower() == "battle")
            FindObjectOfType<MusicPlayer>().PlayBattleMusic();

        SceneManager.LoadScene(name);
    }

    public void QuitRequest()
    {
        #if UNITY_EDITOR
        {
            UnityEditor.EditorApplication.isPlaying = false;
        }
        #endif

        Application.Quit();
    }

    public string CurrentLevelName()
    {
        return SceneManager.GetActiveScene().name;
    }
}
