using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;
using System.IO;
using MoonSharp.Interpreter;
using MoonSharp.Interpreter.Serialization;

public class EditorController : MonoBehaviour
{
    public AccessSaveData accessSaveData;
    public MageEntrySetup mageEntrySetup;
    public CanvasSlider saveWarningCanvas;
    public CanvasSlider jsCanvas;

    public InputField inputField_JS;
    public InputField inputField_mageName;

    public Text errorText;

    public Button saveButton;
    public Button menuButton;

    private bool unsavedEdits;
    private int currentBucket;

    public MageControllerMS prefabMageController;
    private MageControllerMS[] mageControllerMS;

    private int mageArrayCount;

    // Start is called before the first frame update
    void Start()
    {
        mageArrayCount = 0;
        mageControllerMS = new MageControllerMS[mageArrayCount];
        Reload();
    }

    private bool RandomBool()
    {
        if (UnityEngine.Random.Range(0, 2) == 0)
            return true;
        else
            return false;
    }

    private bool ValidateScripts()
    {
        bool valid = true;
        errorText.text = "";

        try
        {
            for (int i = 0; i < mageArrayCount; i++)
            {
                if (mageControllerMS[i] != null)
                {
                    mageControllerMS[i].ExecuteScripts_Raw(inputField_JS.text);
                    mageControllerMS[i].NewTurn();
                }
            }
        }

        catch (ScriptRuntimeException e)
        {
            valid = false;
            errorText.text = e.ToString();
            Debug.Log("VALIDATION FAILED, ScriptRuntimeException was caught: " + e.ToString());
        }

        catch (SyntaxErrorException e)
        {
            valid = false;
            errorText.text = e.ToString();
            Debug.Log("VALIDATION FAILED, Syntax error: " + e.ToString());
        }

        catch (Exception e)
        {
            valid = false;
            errorText.text = e.ToString();
            Debug.Log("VALIDATION FAILED, Generic Exception was caught: " + e.ToString());
        }
        
        return valid;
    }

    public void Reload()
    {
        errorText.text = "";

        currentBucket = mageEntrySetup.CurrentBucket();

        inputField_JS.text = accessSaveData.GetJS(currentBucket);
        inputField_mageName.text = accessSaveData.GetMageName(currentBucket);

        unsavedEdits = false;
    }

    private void ResetTestData()
    {
        for (int i = 0; i < mageControllerMS.Length; i++)
        {
            if (mageControllerMS[i] != null)
                mageControllerMS[i].CleanMage();
        }

        mageControllerMS = new MageControllerMS[mageArrayCount];

        int opponentCount = 0;
        for (int i = 0; i < mageArrayCount; i++)
        {
            mageControllerMS[i] = Instantiate(prefabMageController,
                new Vector3(i, i, 1), transform.rotation);

            //mageControllerMS[i].SetScripts(inputField_JS.text);

            mageControllerMS[i].Start();
            mageControllerMS[i].SetName("editor_" + i.ToString());

            opponentCount++;
        }
    }

    public void SaveEdits()
    {
        bool scriptValidated = true;
        int loopcounter = 0;
        MageData tempMage = new MageData();

        while(scriptValidated && loopcounter < 10)
        {
            mageArrayCount = 10 - loopcounter;

            // load enemies into storage
            ResetTestData();

            // load mage data
            for (int i = 0; i < mageArrayCount; i++)
            {
                if (mageControllerMS[i] != null)
                {
                    for (int j = 0; j < mageArrayCount; j++)
                    {
                        mageControllerMS[i].LoadListData(RandomMageData());
                    }
                    mageControllerMS[i].LoadMyMageData(RandomMageData());
                }
            }
            scriptValidated = ValidateScripts();
            loopcounter++;
        }

        if (scriptValidated)
        {
            accessSaveData.SetJS(currentBucket, inputField_JS.text);
            accessSaveData.SetMageName(currentBucket, inputField_mageName.text);

            SetUnsavedEdits(false);
        }
    }

    private MageData RandomMageData()
    {
        MageData tempMage = new MageData();

        tempMage.id = UnityEngine.Random.Range(1000, 9999);
        tempMage.hp = UnityEngine.Random.Range(1, 11);
        tempMage.mana = UnityEngine.Random.Range(0, 11);
        tempMage.x = UnityEngine.Random.Range(1, 13);
        tempMage.y = UnityEngine.Random.Range(2, 8);
        tempMage.isShieldOn = RandomBool();

        return tempMage;
    }

    public void SetUnsavedEdits(bool setTo)
    {
        unsavedEdits = setTo;
    }

    public bool GetUnsavedEdits()
    {
        return unsavedEdits;
    }

    public void OnBackButton()
    {
        if (unsavedEdits)
        {
            saveWarningCanvas.ActivateCanvas();
        }
        else
            jsCanvas.ExitCanvas();
    }

    public void InitializeJS()
    {
        accessSaveData.ResetMageBucket(currentBucket);
        Reload();
    }
}
