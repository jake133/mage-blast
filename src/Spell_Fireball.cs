using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spell_Fireball : Spell
{
    public Explosion explosion;
    private Transform fireballImage;

    private int angle;
    private int AOEdamage;

    // Start is called before the first frame update
    public void Start()
    {
        fireballImage = GetComponent<Transform>();
        circleCollider = GetComponent<CircleCollider2D>();

        circleCollider.isTrigger = false;
        angle = 1;
        //initialPosition = transform.position;
        show = false;

        damage = 0;
        AOEdamage = 3;
    }

    // Update is called once per frame
    void Update()
    {
        angle++;
        if (angle > 360)
            angle = 1;

        // 2.5 rotations/sec - 900 degrees
        fireballImage.Rotate(new Vector3(
            fireballImage.rotation.x,
            fireballImage.rotation.y,
            1800 * Time.deltaTime));

        if (show)
            MoveBolt();
    }

    public int ManaCost()
    {
        return 1;
    }

    protected override void SpellHitExtras()
    {
        FindObjectOfType<AudioSoundFX>().PlayFireHitFX();

        Instantiate(explosion,
                    new Vector3(
                        transform.position.x,
                        transform.position.y,
                        transform.position.z), transform.rotation);

        int spellHitRadius = 2;

        Collider2D[] hitCollider = Physics2D.OverlapCircleAll(fireballImage.position, spellHitRadius);

        int i = 0;
        while (i < hitCollider.Length)
        {
            GameObject gameObject =  hitCollider[i].gameObject;

            //TODO maybe change this to a gameObject.tag reference?  Doesnt seem to matter
            if (gameObject.name == "mage_dark(Clone)" || gameObject.tag == "Enemy")
            {
                gameObject.GetComponent<CharM3>().ApplyFireDamage(AOEdamage, GetCaster());
            }

            i++;
        }

    }
}
