using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MageEntrySetup : MonoBehaviour
{
    private static int currentBucket = 0;
    private static bool[] bucketActive = new bool[11];
    private static bool[] bucketUseBot = new bool[11];

    void Start()
    {
        /*
        bucketActive = new bool[11];
        bucketUseBot = new bool[11];

        for (int i = 0; i < 11; i++)
        {
            bucketActive[i] = false;
            bucketUseBot[i] = true;
        }
        */
    }

    public void CurrentBucket(int bucket)
    {
        currentBucket = bucket;
    }

    public int CurrentBucket()
    {
        return currentBucket;
    }

    public void SetBucketActive(int bucket)
    {
        bucketActive[bucket] = true;
    }

    public void SetBucketActiveALL()
    {
        for (int i = 1; i<bucketActive.Length; i++)
            bucketActive[i] = true;
    }

    public void SetBucketInactive(int bucket)
    {
        bucketActive[bucket] = false;
    }

    public bool BucketActive(int bucket)
    {
        return bucketActive[bucket];
    }

    public void BucketUseBot(int bucket, bool setTo)
    {
        bucketUseBot[bucket] = setTo;
    }

    public bool BucketUseBot(int bucket)
    {
        return bucketUseBot[bucket];
    }
}