using System.Linq;
using MoonSharp.Interpreter;

public class Data
{
    public MageData[] opponent;
    public MageData me;

    [MoonSharpHidden]
    public void BuildMageData(MageData myMageIn, MageData[] currentArrayIn)
    {
        opponent = new MageData[currentArrayIn.Length];
        opponent = currentArrayIn;
        me = myMageIn;
    }
}