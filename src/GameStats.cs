using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameStats : MonoBehaviour
{
    private List<int> idList = new List<int>();
    private List<string> mageNameList = new List<string>();
    private List<int> killsList = new List<int>();
    private List<int> rank = new List<int>();
    private int killIndex;
    private int totalRanks;

    private bool alreadyCalled = false;

    struct MageRankData
    {
        public int id;
        public string name;
        public int kills;
        public int survRank;
    }
    private List<MageRankData> mageRankData = new List<MageRankData>();

    public void GameEnded()
    {
        int i = 0;
        while (i < idList.Count)
        {
            MageRankData tempMage = new MageRankData
            {
                id = idList[i],
                name = mageNameList[i],
                kills = killsList[i],
                survRank = rank[i]
            };

            mageRankData.Add(tempMage);

            i++;
        }
        // sort by survivability first, as this will NOT change order when sorted by rank, and should allow kill ties to go to the survivor?
        mageRankData.Sort((x,y) => x.survRank.CompareTo(y.survRank));
        mageRankData.Sort((x,y) => y.kills.CompareTo(x.kills));

        
        i = 0;
        while (i < mageRankData.Count)
        {
            Debug.Log("name + kills + survRank: " + mageRankData[i].name + ": " + mageRankData[i].kills + ":" + mageRankData[i].survRank);
            i++;
        }
        
    }
    public void InitMage(int id, string name)
    {
        idList.Add(id);
        mageNameList.Add(name);
        killsList.Add(0);
        rank.Add(0);

        killIndex = idList.Count;
        totalRanks = idList.Count;
    }

    public int GetTotalRanks()
    {
        return totalRanks;
    }

    public void AddKillCount(int id)
    {
        int index = FindIndexByID(id);

        if (index == 999)
            Debug.Log("ID not found - FindIndexByID in GameStats");
        else
            killsList[index]++;
    }

    public void SetRank(int id)
    {
        int index = FindIndexByID(id);

        if (index == 999)
            Debug.Log("ID not found - FindIndexByID in GameStats");
        else if (index < 0)
            Debug.Log("SetDeathOrder - KillIndex invalid - Index is < 0");
        else
        {
            rank[index] = killIndex;
            killIndex--;

            if (killIndex <= 1)
            {
                int i = 0;

                while (i < idList.Count)
                {
                    if (rank[i] == 0)
                        rank[i] = 1;
                    else
                        i++;
                }

                StartCoroutine(PendGameEnded());
            }
        }
    }

    public IEnumerator PendGameEnded()
    {
        if (alreadyCalled)
        { }
        else
        {
            alreadyCalled = true;
            yield return new WaitForSeconds(0.25f);

            GameEnded();
        }
    }

    private int FindIndexByID(int id)
    {
        int i = 0;

        while (i < idList.Count)
        {
            if (idList[i] == id)
                return i;
            else
                i++;
        }

        return 999;
    }

    public string GetNameByRank(int reqRank)
    {
        Debug.Log("GetRank invoked");

        reqRank--; // 1st place is in [0]

        if (reqRank < idList.Count)
            return mageRankData[reqRank].name;
        else
            return "";
    }

    public int GetKillsByRank(int reqRank)
    {
        reqRank--; // 1st place is in [0]

        if (reqRank < idList.Count)
            return mageRankData[reqRank].kills;
        else
            return 0;
    }
}
