using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spell_SwordAttack : Spell
{
    public bool isTimedAttack;
    private CapsuleCollider2D hitCollider;
    private float duration;

    // Start is called before the first frame update
    public void Init()
    {
        duration = 0;
        isTimedAttack = false;
        hitCollider = GetComponent<CapsuleCollider2D>();

        hitCollider.isTrigger = true;
        damage = 1;
    }

    private void Update()
    {
        if (isTimedAttack)
        {
            duration += Time.deltaTime;

            if (duration > 0.25f)
                DestroySpell();
        }
    }
    public int ManaCost()
    {
        return 0;
    }

    public void SwingAttack()
    {
        isTimedAttack = true;
        hitCollider.size = new Vector2(1.5f, 1.5f);

    }
}
