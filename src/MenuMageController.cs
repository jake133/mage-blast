using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MenuMageController : MonoBehaviour
{
    private EAction action;
    private float turnDuration = 2;
    private float turnTimer;
    private bool movedThisTurnAlready;
    private Vector2 centerScreen;
    public CharM3_Mage mage;

    // Start is called before the first frame update
    void Start()
    {
        centerScreen = new Vector2(8.5f, 6.5f);
        turnTimer = -2;
        movedThisTurnAlready = false;
    }

    // Update is called once per frame
    void Update()
    {
        turnTimer += Time.deltaTime;

        if (turnTimer > turnDuration)
        {
            MageAction();

            turnTimer = 0;
            movedThisTurnAlready = false;
        }
        else if (turnTimer > (turnDuration / 2) + 0.1f && !movedThisTurnAlready)
        {   // added 0.1f to timer above to allow cast spell to start firing before player moves
            MoveMage();

            movedThisTurnAlready = true;
        }
        else
        {
            // do nothing
        }
    }

    private void MoveMage()
    {
        if (mage.GetLocation().x < 2)
        {   // if near left wall
            mage.TakeMoveAction(90);
        }
        else if (mage.GetLocation().x > (FindObjectOfType<Camera>().transform.position.x * 2) - 2)
        { // if near right wall
            mage.TakeMoveAction(270);
        }
        else
        {   // in the middle
            switch (UnityEngine.Random.Range(1, 3))
            {
                case 1:
                    mage.TakeMoveAction(90);
                    break;
                default:
                    mage.TakeMoveAction(270);
                    break;
            }
        }
    }

    private void MageAction()
    {
        if (mage.GetMana() < 5)
        {
            action = EAction.nothing;
            mage.TakeSpellAction(action, new Vector2(5, 5));
        }
        else
        {
            switch (UnityEngine.Random.Range(1, 7))
            {
                case 1:
                    action = EAction.blink;
                    if (UnityEngine.Random.Range(1, 3) == 1)
                        mage.CastBlinkAction(90);
                    else
                        mage.CastBlinkAction(270);
                    break;
                case 2:
                    action = EAction.drainmana;
                    mage.TakeSpellAction(action, centerScreen);
                    break;
                case 3:
                    action = EAction.fireball;
                    mage.TakeSpellAction(action, centerScreen);
                    break;
                case 4:
                    action = EAction.frostbolt;
                    mage.TakeSpellAction(action, centerScreen);
                    break;
                case 5:
                    action = EAction.snowball;
                    mage.TakeSpellAction(action, centerScreen);
                    break;
                default:
                    action = EAction.nothing;
                    mage.TakeSpellAction(action, centerScreen);
                    break;
            }
        }
    }
}
