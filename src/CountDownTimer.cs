using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CountDownTimer : MonoBehaviour
{
    public Text timerText;
    public AudioSource timerFX;
    public AudioSource goFX;
    private Color originalTimerColor;
    private bool keepFading;

    // Start is called before the first frame update
    void Start()
    {
        originalTimerColor = timerText.color;
        keepFading = true;

        StartCoroutine(CountDown());
    }

    private IEnumerator CountDown()
    {
        yield return new WaitForSeconds(1);
        timerText.text = "3";
        timerFX.Play();
        timerText.color = new Color(originalTimerColor.r, originalTimerColor.g, originalTimerColor.b, 1);
        yield return new WaitForSeconds(2);

        timerText.text = "2";
        timerFX.Play();
        timerText.color = new Color(originalTimerColor.r, originalTimerColor.g, originalTimerColor.b, 1);
        yield return new WaitForSeconds(2);

        timerText.text = "1";
        timerFX.Play();
        timerText.color = new Color(originalTimerColor.r, originalTimerColor.g, originalTimerColor.b, 1);
        yield return new WaitForSeconds(2);

        timerText.text = "Go!";
        goFX.Play();
        timerText.color = new Color(originalTimerColor.r, originalTimerColor.g, originalTimerColor.b, 1);
        yield return new WaitForSeconds(2);

        keepFading = false;

    }
    // Update is called once per frame
    void Update()
    {
        if (keepFading)
        {
            timerText.color = new Color(timerText.color.r, timerText.color.g, timerText.color.b,
                timerText.color.a - Time.deltaTime/2);
        }
        else
        {
            timerText.enabled = false;
        }
    }
}
