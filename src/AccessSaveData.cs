using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using System;
using System.IO;
using System.Runtime.InteropServices;
using System.Runtime.Serialization.Formatters.Binary;

/*** AccessSaveData ***
 * Requires: 
 * Function: Controls access to saved coins, bonus coins, and brain badges. Can update those fields as well.
 * 
 * Notes: Must be initialized prior to Solo Start Controller to ensure coins are reset correctly if timer
 * expired while offline
 */

public class AccessSaveData : MonoBehaviour
{
    public LevelManager levelManager;

    public string tempMageName;
    public TextAsset defaultJS;

    private static SaveData[] saveData = new SaveData[11];
    private static string[] savePath = new string[11];

    private static SaveData[] localSaveData = new SaveData[11];
    private static bool loadedLocalSaveData = false;

    private static int currentBucket = 0;
    private static bool[] bucketActive = new bool[11];
    private static bool[] bucketUseBot = new bool[11];

    void Start()
    {
        // Application data location used to save game data for later use

        //saveData = new SaveData[11];
        //savePath = new string[11];
        //localSaveData = new SaveData[11];
        //loadedLocalSaveData = new bool[11];
        //bucketUseBot = new bool[11];
        //bucketActive = new bool[11];

        if (!loadedLocalSaveData)
        {
            for (int i = 0; i < 11; i++)
            {
                saveData[i] = new SaveData();
                localSaveData[i] = saveData[i];

                SetPersistentPaths(i);
                //loadedLocalSaveData[i] = false;
                bucketUseBot[i] = true;
                bucketActive[i] = false;

                // TEsTING ONLY CAUSE WEBGL SAVING SUCKS!
                //saveData[i] = new SaveData();
                saveData[i].mageName = tempMageName + " " + i;
                saveData[i].customJS = defaultJS.ToString();
                // END TESTING

                localSaveData[i] = GetSaveData(i);
                loadedLocalSaveData = true;
            }
        }
    }

    public void SetCurrentBucket(int bucket)
    {
        currentBucket = bucket;
    }

    public int GetCurrentBucket()
    {
        return currentBucket;
    }

    private void SetPersistentPaths(int bucket)
    {
        savePath[bucket] = "";
        //savePath[bucket] = string.Format("{0}/mScript_" + bucket + ".dat", Application.persistentDataPath);
        //savePath[bucket] = Application.persistentDataPath + "/jsScript_" + bucket + ".txt";
    }

    public string GetJS(int bucket)
    {
        //return "";
        return localSaveData[bucket].customJS;
    }

    public void SetJS(int bucket, string JSin)
    {
        saveData[bucket] = GetSaveData(bucket);
        saveData[bucket].customJS = JSin;

        UpdateSaveData(bucket, saveData[bucket]);
    }

    public string GetMageName(int bucket)
    {
        //return "name" + bucket;
        return localSaveData[bucket].mageName;
    }

    public void SetMageName(int bucket, string nameIn)
    {
        saveData[bucket] = GetSaveData(bucket);
        saveData[bucket].mageName = nameIn;

        UpdateSaveData(bucket, saveData[bucket]);
    }

    public void ResetMageBucket(int bucket)
    {
        saveData[bucket].mageName = tempMageName + " " + bucket;
        saveData[bucket].customJS = defaultJS.ToString();

        UpdateSaveData(bucket, saveData[bucket]);
    }

    public void ResetAllSaveData()
    {

        saveData = new SaveData[11];
        localSaveData = saveData;

        for (int i = 0; i < 11; i++)
        {
            UpdateSaveData(i, saveData[i]);
        }

        levelManager.LoadLevel("Menu");
    }

    #region file access
    // ** file accessing methods ***

    private SaveData GetSaveData(int bucket)
    {
        /*
        return saveData[bucket];

        FileStream fileStream;
        BinaryFormatter bf = new BinaryFormatter();

        if (File.Exists(savePath[bucket]))
        {

            Debug.Log("save data file EXISTS: " + savePath[bucket]);

            //BinaryFormatter bf = new BinaryFormatter();
            fileStream = File.Open(savePath[bucket], FileMode.Open);

            saveData[bucket] = (SaveData)bf.Deserialize(fileStream);
            fileStream.Close();
        }
        else
        {
            Debug.Log("save data file NEW: " + savePath[bucket]);

            saveData[bucket] = new SaveData();

            for (int i = 0; i < 10; i++)
            {
                saveData[bucket].mageName = tempMageName + " " +  bucket;
                saveData[bucket].customJS = defaultJS.ToString();
            }

            //BinaryFormatter bf = new BinaryFormatter();
            fileStream = File.Create(savePath[bucket]);
            bf.Serialize(fileStream, saveData[bucket]);
            fileStream.Close();
        }

        //  if (Application.platform == RuntimePlatform.WebGLPlayer)
        //  {
        //      SyncFiles();
        //  }
        Debug.Log("Exiting GetSaveData function");
        */
        return saveData[bucket];
    }

    private void UpdateSaveData(int bucket, SaveData saveData)
    {
        localSaveData[bucket] = saveData;
        /*
        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = File.Open(savePath[bucket], FileMode.Open);
        bf.Serialize(file, saveData);
        file.Close();
        */
    }

    #endregion File Access
}
