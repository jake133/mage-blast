using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shadow : MonoBehaviour
{
    private float startingY;
    private float scaleFactor;
    private float currentFactor;
    private SpriteRenderer shadowImage;

    // Start is called before the first frame update
    void Start()
    {
        startingY = transform.position.y;
        scaleFactor = transform.parent.transform.position.y - transform.position.y;
        shadowImage = GetComponent<SpriteRenderer>();

    }

    // Update is called once per frame
    void Update()
    {
        currentFactor = transform.parent.transform.position.y - transform.position.y;

        transform.position = new Vector2(transform.position.x, startingY);
        transform.localScale = new Vector3(Mathf.Clamp(scaleFactor / currentFactor, 0, 1), 
            Mathf.Clamp(scaleFactor / currentFactor, 0, 1), 1);


        shadowImage.color = new Color(shadowImage.color.r, 
            shadowImage.color.g, 
            shadowImage.color.b,
            Mathf.Clamp(scaleFactor / currentFactor, 0, 1));
    }

    public void OnNewFloor()
    {
        startingY = transform.parent.transform.position.y - scaleFactor;
    }
}
