using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HP_ManaDisplay_Platform : MonoBehaviour
{
    public CharM3_Mage_Platformer charM3_platformer;
    public Transform hpSprite;
    public Transform manaSprite;

    private Vector3 originalHPScale;
    private Vector3 originalManaScale;

    // Start is called before the first frame update
    void Start()
    {
        //charM3_platformer = GetComponentInParent<CharM3_Mage_Platformer>();

        if (hpSprite != null)
            originalHPScale = hpSprite.transform.localScale;
        if (manaSprite != null)
            originalManaScale = manaSprite.transform.localScale;
    }

    public void Update()
    {
        //show image at scale hp/maxHp & mana/maxMana
        if (hpSprite != null && charM3_platformer != null)
            hpSprite.transform.localScale = new Vector3(
                originalHPScale.x * ((float)charM3_platformer.GetHP() / charM3_platformer.GetMaxHp()),
                originalHPScale.y, 
                originalHPScale.z);


        if (manaSprite != null && charM3_platformer != null)
            manaSprite.transform.localScale = new Vector3(
                originalManaScale.x * ((float)charM3_platformer.GetMana() / charM3_platformer.GetMaxMana()), 
                originalManaScale.y, originalManaScale.z);
    }
}
