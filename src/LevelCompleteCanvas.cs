using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LevelCompleteCanvas : MonoBehaviour
{
    public GameStats gameStats;
    public Canvas completeCanvas;
    public Text[] playerName;
    public Text[] kills;

    private float startingPosition;
    private float lerpToPosition;

    private bool alreadyLoaded;

    // Use this for initialization
    void Start()
    {
        alreadyLoaded = false;
        startingPosition = Screen.height * 2;

        lerpToPosition = startingPosition;
    }

    private void Update()
    {
        LerpCanvas(lerpToPosition);
    }

    public void ActivateCompletionCanvas()
    {
        if (alreadyLoaded)
        { }
        else
        {
            alreadyLoaded = true;
            StartCoroutine(LoadCanvasElements());
        }
    }

    private IEnumerator LoadCanvasElements()
    {
        yield return new WaitForSeconds(0.6f);
        int i = 0;

        while (i < gameStats.GetTotalRanks())
        {
            playerName[i].text = gameStats.GetNameByRank(i + 1);
            kills[i].text = gameStats.GetKillsByRank(i + 1).ToString();

            i++;
        }

        lerpToPosition = 0;
    }

    public void ExitCompletionCanvas()
    {
        lerpToPosition = startingPosition;
    }

    private void LerpCanvas(float moveToY)
    {
        float newY = Mathf.Lerp(completeCanvas.GetComponent<RectTransform>().anchoredPosition.y,
            moveToY, Time.deltaTime * 12f);
        Vector2 newPosition = new Vector2(completeCanvas.GetComponent<RectTransform>().anchoredPosition.x, newY);

        completeCanvas.GetComponent<RectTransform>().anchoredPosition = newPosition;
    }
}

