
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioSoundFX : MonoBehaviour
{
    public AudioSource[] frostBoltFX;
    public AudioSource[] snowBallFX;
    public AudioSource drainManaFX;
    public AudioSource iceShieldFX;
    public AudioSource blinkFX;
    public AudioSource[] onHitFX;
    public AudioSource onDeathFX;
    public AudioSource iceShieldRemovedFX;
    public AudioSource[] fizzleFX;
    public AudioSource fireballCastFX;
    public AudioSource fireballHitFX;

    // Use this for initialization
    void Start()
    {
    }

    // Update is called once per frame

    public void PlayFrostBoltFX()
    {
        switch (Random.Range(0, frostBoltFX.Length))
        {
            case 0:
                frostBoltFX[0].Play();
                break;
            case 1:
                frostBoltFX[1].Play();
                break;
            case 2:
                frostBoltFX[2].Play();
                break;
            default:
                frostBoltFX[frostBoltFX.Length - 1].Play();
                break;
        }
    }

    public void PlaySnowBallFX()
    {
        switch (Random.Range(0, snowBallFX.Length))
        {
            case 0:
                snowBallFX[0].Play();
                break;
            case 1:
                snowBallFX[1].Play();
                break;
            case 2:
                snowBallFX[2].Play();
                break;
            default:
                snowBallFX[snowBallFX.Length - 1].Play();
                break;
        }
    }

    public void PlayDrainManaFX()
    {
        drainManaFX.Play();
    }

    public void PlayIceShieldFX()
    {
        iceShieldFX.Play();
    }

    public void PlayBlinkFX()
    {
        blinkFX.Play();
    }

    public void PlayFireCastFX()
    {
        fireballCastFX.Play();
    }

    public void PlayFireHitFX()
    {
        fireballHitFX.Play();
    }

    public void PlayOnHitFX()
    {
        switch (Random.Range(0, onHitFX.Length))
        {
            case 0:
                onHitFX[0].Play();
                break;
            case 1:
                onHitFX[1].Play();
                break;
            case 2:
                onHitFX[2].Play();
                break;
            case 3:
                onHitFX[2].Play();
                break;
            default:
                onHitFX[onHitFX.Length - 1].Play();
                break;
        }

    }

    public void PlayOnDeathFX()
    {
        onDeathFX.Play();
    }

    public void PlayIceShieldRemovedFX()
    {
        iceShieldRemovedFX.Play();
    }

    public void PlayFizzleFX()
    {
        switch (Random.Range(0, fizzleFX.Length))
        {
            case 0:
                fizzleFX[0].Play();
                break;
            case 1:
                fizzleFX[1].Play();
                break;
            case 2:
                fizzleFX[2].Play();
                break;
            default:
                fizzleFX[fizzleFX.Length - 1].Play();
                break;
        }

    }
}
