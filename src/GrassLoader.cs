using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GrassLoader : MonoBehaviour
{
    public Grass grass;

    private float x;
    private float y;

    private float altY;

    // Start is called before the first frame update
    void Start()
    {
        x = 0;
        y = 1.5f;

        altY = 0;

        while (y < 10)
        {
            while (x < 18)
            {
                altY = Random.Range(-0.25f, 0.25f);
                Instantiate(grass, new Vector3(x, y + altY, 1), transform.rotation);

                x = x + Random.Range(0.25f, 0.5f);
            }

            y = y + Random.Range(0.25f, 0.5f);
            x = 0;
        }
    }
}
