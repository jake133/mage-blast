using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Floor : MonoBehaviour
{
    public CharM3_Mage_Platformer charM3_Mage_Platformer;

    // Start is called before the first frame update
    void Start()
    {
        charM3_Mage_Platformer = FindObjectOfType<CharM3_Mage_Platformer>();
    }

    // Update is called once per frame
    void Update()
    {

    }


    protected virtual void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.collider.tag == "Player")
        {
            charM3_Mage_Platformer.NoLongerJumping();
        }
    }
}
